package suitebot3.methods;

import org.junit.Assert;
import org.junit.Test;
import suitebot3.ai.SampleBotAi;
import suitebot3.game.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActionTest {

    private static final int MY_ID = 1;

    @Test
    public void calculatePointsInMiddlePlan() {

        GamePlan fakeGamePlan = new StandardGamePlan(100, 50, getStartingPositions());
        GameSetup fakeGameSetup = new GameSetup(MY_ID, getFakePlayers(), fakeGamePlan);
        SampleBotAi bot = new SampleBotAi(fakeGameSetup);
        GameState fakeState = new StandardGameState(fakeGamePlan, getFakePlayers());

        int calculated = bot.calculatePoints(20, 20, fakeState);
        Assert.assertEquals(17, calculated);

    }

    @Test
    public void calculatePointsNextBorders() {

        GamePlan fakeGamePlan = new StandardGamePlan(100, 50, getStartingPositions());
        GameSetup fakeGameSetup = new GameSetup(MY_ID, getFakePlayers(), fakeGamePlan);
        SampleBotAi bot = new SampleBotAi(fakeGameSetup);
        GameState fakeState = new StandardGameState(fakeGamePlan, getFakePlayers());

        int calculated = bot.calculatePoints(2, 20, fakeState);
        Assert.assertEquals(9, calculated);

    }

    private Map<Integer, Point> getStartingPositions() {
        Map<Integer, Point> startingPositions = new HashMap<>();
        startingPositions.put(1, new Point(5, 12));
        startingPositions.put(2, new Point(15, 12));
        startingPositions.put(3, new Point(25, 12));
        startingPositions.put(4, new Point(35, 12));
        startingPositions.put(5, new Point(45, 36));
        startingPositions.put(6, new Point(55, 36));
        startingPositions.put(7, new Point(65, 36));
        startingPositions.put(8, new Point(75, 36));
        return startingPositions;
    }

    private List<Player> getFakePlayers() {
        List<Player> fakePlayers = new ArrayList<>();
        fakePlayers.add(new Player(MY_ID, "Ja"));
        fakePlayers.add(new Player(2, "Karel"));
        fakePlayers.add(new Player(3, "Jozka"));
        fakePlayers.add(new Player(4, "Bozena"));
        fakePlayers.add(new Player(5, "Picus"));
        fakePlayers.add(new Player(6, "Vojeb"));
        fakePlayers.add(new Player(7, "Tabule"));
        fakePlayers.add(new Player(8, "Stanislav"));
        return fakePlayers;
    }
}