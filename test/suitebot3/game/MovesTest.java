package suitebot3.game;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class MovesTest
{
	@Test
	public void step1HasTheValueSetInTheConstructor()
	{
		Action action = Action.DOWN;
		Moves moves = new Moves(action);

		assertThat(moves.getAction(), is(action));
	}

	@Test
	public void sameMovesShouldEqual()
	{
		assertThat(new Moves(Action.RIGHT), equalTo(new Moves(Action.RIGHT)));
		assertThat(new Moves(), equalTo(new Moves()));
		assertThat(new Moves(Action.HOLD), equalTo(new Moves()));
	}

	@Test
	public void differentMovesShouldNotEqual()
	{
		assertThat(new Moves(Action.RIGHT), not(equalTo(new Moves(Action.LEFT))));
		assertThat(new Moves(null), not(equalTo(new Moves(Action.LEFT))));
	}

	@Test
	public void holdDoesNotMove()
	{
		assertThat(Action.HOLD.asString, equalTo("H"));
		assertThat(Action.HOLD.dx, equalTo(0));
		assertThat(Action.HOLD.dy, equalTo(0));
	}

	@Test
	public void notMovingSerializedProperly()
	{
		String serialized =	new Moves().serialize();
		assertThat(serialized, is("H"));
	}
}