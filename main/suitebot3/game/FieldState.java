package suitebot3.game;

public interface FieldState
{
    /** Get number of resources available on the field */
    int getResourceCount();

    /** Get an agent standing on the field or null. */
    Agent getAgent();

    /** Get owner a home base located on the field or null. */
    Player getBase();

    /** Get a bomb planted on the field or null. */
    Bomb getBomb();
}
