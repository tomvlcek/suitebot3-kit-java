package suitebot3.game;

import com.google.common.base.Objects;

public class UpdatableFieldState implements FieldState
{
    private UpdatableAgent agent;
    private int resources;
    private final Player base;
    private Bomb bomb;

    public UpdatableFieldState(Player base, int initialResources)
    {
        agent = null;
        this.base = base;
        resources = initialResources;
        
        if (base != null && resources != 0)
            throw new IllegalArgumentException("It is not possible to have resources where home base is");
    }

    @Override
    public int getResourceCount()
    {
        return resources;
    }

    @Override
    public Agent getAgent()
    {
        return agent;
    }

    @Override
    public Player getBase()
    {
        return base;
    }

    public UpdatableAgent getAgentUpdatable()
    {
        return agent;
    }

    public void setAgent(UpdatableAgent agent)
    {
        this.agent = agent;
    }

    public Bomb getBomb()
    {
        return bomb;
    }

    public void setBomb(Bomb bomb)
    {
        this.bomb = bomb;
    }

    public void setResources(int resources)
    {
        if (resources > GameConstants.FIELD_MAX_RESOURCES)
            throw new IllegalStateException("Cannot set more resources than " + GameConstants.FIELD_MAX_RESOURCES);
        this.resources = resources;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdatableFieldState that = (UpdatableFieldState) o;
        return resources == that.resources &&
                agent == null ? that.agent == null : Objects.equal(agent.getPlayer(), that.agent.getPlayer()) &&
                Objects.equal(base, that.base) &&
                Objects.equal(bomb, that.bomb);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(agent, resources, base, bomb);
    }
}
