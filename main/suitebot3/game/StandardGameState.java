package suitebot3.game;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import suitebot3.game.dto.GameStateDTO;
import suitebot3.game.dto.PlayerDTO;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

public class StandardGameState implements GameState
{
	private final GamePlan gamePlan;
	private final List<Player> players;
	private UpdatableFieldState[][] fields = null;
	private List<Point> positions;

	private final Map<Integer, PlayerState> playerStateMap = new HashMap<>();
	protected int roundsRemaining;
	protected int currentRound;

	public StandardGameState(GamePlan gamePlan, Player... players)
	{
		this(gamePlan, Arrays.asList(players));
	}

	public StandardGameState(GamePlan gamePlan, List<Player> players, boolean putAgentsToHomeBases) {
		this(gamePlan, players);
		if (putAgentsToHomeBases)
			this.putAgentsToHomeBases();
	}

	public StandardGameState(GamePlan gamePlan, List<Player> players)
	{
		this.gamePlan = new StandardGamePlan(gamePlan);
		this.players = ImmutableList.copyOf(players);
		this.roundsRemaining = gamePlan.getMaxRounds();
		this.currentRound = 1;
		initPositions();

		assertPlayersUnique();
		assertPlayerIdsNotZero();

		if (players.size() != gamePlan.getHomeBasePositions().size())
		{
			throw new IllegalStateException(String.format("number of players (%d) is different from the number of starting positions (%d)",
					players.size(), gamePlan.getHomeBasePositions().size()));
		}

		for (Player player : players)
		{
			PlayerState playerState = new PlayerState(player);
			playerStateMap.put(player.id, playerState);
		}

		fields = new UpdatableFieldState[gamePlan.getWidth()][gamePlan.getHeight()];
		clearFields(getHomeBaseMap());
	}

	public StandardGameState(GameState gameState)
	{
		this.gamePlan = new StandardGamePlan(gameState.getGamePlan());
		this.players = ImmutableList.copyOf(gameState.getPlayers());
		this.roundsRemaining = gameState.getRoundsRemaining();
		this.currentRound = gameState.getCurrentRound();
		initPositions();

		assertPlayersUnique();
		assertPlayerIdsNotZero();

		for (Player player : players)
		{
			PlayerState playerState = new PlayerState(player);
			playerStateMap.put(player.id, playerState);
			playerState.setResources(gameState.getPlayerResources(playerState.getPlayer().id));
		}

		fields = new UpdatableFieldState[gamePlan.getWidth()][gamePlan.getHeight()];
		clearFields(getHomeBaseMap());

		// copy field resources
		gameState.allFields().forEach(fieldPos -> {
			UpdatableFieldState field = getFieldUpdatable(fieldPos.position);
			field.setResources(fieldPos.fieldState.getResourceCount());

			Bomb bomb = fieldPos.fieldState.getBomb();
			if (bomb != null)
				field.setBomb(new Bomb(bomb));
		});

		// spawn
		for (Player otherPlayer : gameState.getPlayers())
		{
			Agent otherAgent = gameState.getAgentOfPlayer(otherPlayer.id);
			if (otherAgent != null)
				spawnAgent(otherAgent.getPosition(), getPlayerState(otherPlayer.id).getPlayer().id);
		}
	}

	public StandardGameState(GamePlan gamePlan, List<Player> players, int currentRound, int remainingRounds)
	{
		this(gamePlan, players);
		this.currentRound = currentRound;
		this.roundsRemaining = remainingRounds;
	}

	@Override
	public Stream<FieldPos> allFields()
	{
		return positions.stream()
				.map(position -> new FieldPos(position, fields[position.x][position.y]));
	}

	@Override
	public boolean isGameOver()
	{
		return roundsRemaining <= 0;
	}

	@Override
	public int getCurrentRound()
	{
		return currentRound;
	}

	@Override
	public int getRoundsRemaining()
	{
		return roundsRemaining;
	}

	@Override
	public Agent getAgentOfPlayer(int playerId)
	{
		return getPlayerState(playerId).getAgent();
	}

	@Override
	public int getPlayerResources(int playerId)
	{
		return getPlayerState(playerId).getResources();
	}

	/**
	 * Preferably, use the following methods to manipulate player state:
	 *
	 *   Agent spawnAgent(Point position, int playerId);
	 *   void killAgent(Agent agent);
	 *   void moveAgent(Agent agent, Point targetPosition);
	 *
	 *   void setResourcesOnField(Point position, int resources);
	 *   void setPlayerResources(int playerId, int resources);
	 */
	protected PlayerState getPlayerState(int playerId)
	{
		checkThatPlayerIdIsCorrect(playerId);
		return playerStateMap.get(playerId);
	}

	private void checkThatPlayerIdIsCorrect(int playerId)
	{
		if (!playerStateMap.containsKey(playerId))
			throw new RuntimeException("No player with id " + playerId);
	}

	@Override
	public List<Player> getPlayers()
	{
		return players;
	}

	@Override
	public GamePlan getGamePlan()
	{
		return gamePlan;
	}

	@Override
	public FieldState getField(Point position)
	{
		gamePlan.checkIsOnPlan(position);
		return fields[position.x][position.y];
	}

	@Override
	public Agent spawnAgent(Point position, int playerId)
	{
		UpdatableAgent agent = getPlayerState(playerId).spawnAgent(position);
		getFieldUpdatable(position).setAgent(agent);
		return agent;
	}

	@Override
	public void killAgent(Agent agent)
	{
		getFieldUpdatable(agent.getPosition()).setAgent(null);
		getPlayerState(agent.getPlayer().id).killAgent();
	}

	@Override
	public void moveAgent(Agent agent, Point targetPosition)
	{
		UpdatableAgent updatableAgent = (UpdatableAgent) agent;

		UpdatableFieldState sourceField = getFieldUpdatable(agent.getPosition());
		if (sourceField.getAgent() == agent)
			getFieldUpdatable(agent.getPosition()).setAgent(null);

		updatableAgent.setPosition(targetPosition);
		getFieldUpdatable(agent.getPosition()).setAgent(updatableAgent);
	}

	@Override
	public void setResourcesOnField(Point position, int resources)
	{
		getFieldUpdatable(position).setResources(resources);
	}

	@Override
	public void setPlayerResources(int playerId, int resources)
	{
		getPlayerState(playerId).setResources(resources);
	}

	@Override
	public void setBombOnField(Point position, Bomb bomb)
	{
		getFieldUpdatable(position).setBomb(bomb);
	}

	@Override
	public GameStateDTO toDto()
	{
		GameStateDTO dto = new GameStateDTO();

		dto.currentRound = getCurrentRound();
		dto.remainingRounds = roundsRemaining;

		dto.gamePlanWidth = gamePlan.getWidth();
		dto.gamePlanHeight = gamePlan.getHeight();

		dto.players = new HashMap<>(players.size());
		for (Player player : players)
		{
			PlayerState playerState = getPlayerState(player.id);

			PlayerDTO playerDTO = new PlayerDTO();
			playerDTO.name = player.name;
			playerDTO.homeBaseLocation = gamePlan.getHomeBasePositions().get(player.id);
			playerDTO.agentLocation = playerState.isAgentAlive() ? playerState.getAgent().getPosition() : null;
			playerDTO.agentLastMove = playerState.isAgentAlive() ? playerState.getAgent().getLastMove() : null;
			playerDTO.resources = playerState.getResources();

			dto.players.put(player.id, playerDTO);
		}
		
		dto.fieldResources = createFields(p -> getField(p).getResourceCount());
		dto.fieldBombs = createFields(p -> {
			Bomb bomb = getField(p).getBomb();
			return bomb == null ? null : bomb.toDto();
		});

		return dto;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		StandardGameState gameState = (StandardGameState) o;

		return  roundsRemaining == gameState.roundsRemaining &&
				currentRound == gameState.currentRound &&
				Objects.equal(gamePlan, gameState.gamePlan) &&
				Objects.equal(players, gameState.players) &&
				Arrays.deepEquals(fields, gameState.fields) &&
				Objects.equal(positions, gameState.positions) &&
				Objects.equal(playerStateMap, gameState.playerStateMap);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(gamePlan, players, fields, positions, playerStateMap, roundsRemaining, currentRound);
	}

	private <T> List<List<T>> createFields(Function<Point, T> getter)
	{
		List<List<T>> fieldValues = new ArrayList<>(gamePlan.getWidth());
		
		for (int x = 0; x < gamePlan.getWidth(); x++)
		{
			List<T> column = new ArrayList<>(gamePlan.getHeight());
			fieldValues.add(column);

			for (int y = 0; y < gamePlan.getHeight(); y++)
				column.add(getter.apply(new Point(x, y)));
		}
		
		return fieldValues;
	}
	
	
	protected Stream<UpdatableFieldPos> allFieldsUpdatable()
	{
		return positions.stream()
				.map(position -> new UpdatableFieldPos(position, fields[position.x][position.y]));
	}

	protected UpdatableFieldState getFieldUpdatable(Point position)
	{
		return fields[position.x][position.y];
	}

	private void putAgentsToHomeBases()
	{
		allFieldsUpdatable()
				.filter(fieldPos -> fieldPos.fieldState.getBase() != null)
				.forEach(fieldPos -> {
					if (fieldPos.fieldState.getAgent() != null)
						throw new RuntimeException("Cannot put an agent to base " + fieldPos.position + ", there is an agent already.");

					spawnAgent(fieldPos.position, fieldPos.fieldState.getBase().id);
				});
	}

	private Map<Point, Player> getHomeBaseMap()
	{
		Map<Point, Player> homeBaseMap = new HashMap<>(players.size());

		for (Player player : players) {
			Point homeBasePosition = gamePlan.getHomeBasePositions().get(player.id);
			homeBaseMap.put(homeBasePosition, player);
		}

		return homeBaseMap;
	}

	private void clearFields(Map<Point, Player> homeBases)
	{
		for (int x = 0; x < gamePlan.getWidth(); x++)
		{
			for (int y = 0; y < gamePlan.getHeight(); y++)
			{
				Point position = new Point(x, y);
				Player baseOwner = homeBases.get(position);

				int initialResources = baseOwner == null ? gamePlan.getInitialResources(new Point(x, y)) : 0;
				UpdatableFieldState field = new UpdatableFieldState(baseOwner, initialResources);
				fields[x][y] = field;
			}
		}
	}

	private void initPositions()
	{
		positions = new ArrayList<>(gamePlan.getWidth() * gamePlan.getHeight());

		for (int x = 0; x < gamePlan.getWidth(); ++x)
		{
			for (int y = 0; y < gamePlan.getHeight(); ++y)
				positions.add(new Point(x, y));
		}
	}

	private void assertPlayersUnique()
	{
		Set<Player> uniquePlayers = ImmutableSet.copyOf(players);
		if (uniquePlayers.size() < players.size())
			throw new IllegalStateException("Players are not unique");
	}

	private void assertPlayerIdsNotZero()
	{
		for (Player player : players)
		{
			if (player.id == 0)
				throw new IllegalStateException("Player ID cannot be 0");
		}
	}

	protected static class UpdatableFieldPos
	{
		public final Point position;
		public final UpdatableFieldState fieldState;

		public UpdatableFieldPos(Point position, UpdatableFieldState fieldState)
		{
			this.position = position;
			this.fieldState = fieldState;
		}
	}
}
