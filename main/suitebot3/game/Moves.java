package suitebot3.game;

/**
 * Class to record the moves your agent should do.
 * Use inner class Moves.Builder to create an instance easily.
 */
public class Moves
{
	private final Action action;

	public Moves(Action action)
	{
		this.action = action;
	}

    public Moves() {
        this(Action.HOLD);
    }

	public String serialize()
	{
		return toString();
	}

	public Action getAction()
	{
		return action;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Moves moves = (Moves) o;
		return action == moves.action;
	}

	@Override
	public int hashCode()
	{
		return action.hashCode();
	}

	@Override
	public String toString()
	{
		if (action == null)
			return Action.HOLD.toString();
		else
			return action.toString();
	}
}
