package suitebot3.game;

public class GameConstants
{
    public static final int ROUND_COUNT = 100;
    
    public static final int PLAYER_INITIAL_RESOURCES = 0;
    public static final int AGENT_CONSUMPTION_RATE = 0;
    public static final int AGENT_COLLECTION_RATE = 0;

    public static final int GROW_RATE = 0;
    public static final int GROW_THRESHOLD = 20;
    public static final int FIELD_INITIAL_RESOURCES = 1;
    public static final int FIELD_MAX_INITIAL_RESOURCES = 1;
    public static final int FIELD_MAX_RESOURCES = 1;

    public static final int BOMB_COUNTDOWN = 4;
    public static final int SCORE_FOR_KILLING_ENEMY = 10;
}
