package suitebot3.game;

import com.google.common.base.Objects;

public class PlayerState
{
	private UpdatableAgent agent;
	private final Player player;
	private int resources;

	public PlayerState(Player player)
	{
		this.player = player;
		agent = null;
		resources = GameConstants.PLAYER_INITIAL_RESOURCES;
	}

	public Player getPlayer()
	{
		return player;
	}

	public boolean isAgentAlive()
	{
		return agent != null;
	}

	public Agent getAgent()
	{
		return agent;
	}

	public UpdatableAgent getUpdatableAgent()
	{
		return agent;
	}

	public UpdatableAgent spawnAgent(Point position)
	{
		if (agent != null)
			throw new IllegalStateException("The player already has an agent");

		agent = new PlayersAgent(position, player);
		return agent;
	}

	public void killAgent()
	{
		agent = null;
	}

	public int getResources()
	{
		return resources;
	}

	public void setResources(int resources)
	{
		this.resources = resources;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		PlayerState that = (PlayerState) o;
		return resources == that.resources &&
				Objects.equal(getAgentPosition(), ((PlayerState) o).getAgentPosition()) &&
				Objects.equal(player, that.player);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(agent, player, resources);
	}

	private Point getAgentPosition()
	{
		if (agent == null)
			return null;
		else
			return agent.getPosition();
	}

	private class PlayersAgent implements UpdatableAgent
	{
		private Point position;
		private final Player owner;
		private Action lastMove;

		private PlayersAgent(Point position, Player owner)
		{
			this.position = position;
			this.owner = owner;
		}

		@Override
		public void setPosition(Point position)
		{
			this.position = position;
		}

		@Override
		public Point getPosition()
		{
			return position;
		}

		@Override
		public Player getPlayer()
		{
			return owner;
		}

		@Override
		public void kill()
		{
			killAgent();
		}

		@Override
		public Action getLastMove()
		{
			return lastMove;
		}

		@Override
		public void setLastMove(Action lastMove)
		{
			this.lastMove = lastMove;
		}
	}
}
