package suitebot3.ai;

import suitebot3.game.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class SampleBotAi implements BotAi {

    public static final int ROUND_STEPS = 5;
    public static final int STRATEGIES = 4;

    public int myId;

    public Point startPossition;
    public int strategy = 0;

    public SampleBotAi(GameSetup gameSetup) {

        myId = gameSetup.aiPlayerId;

        Map<Integer, Point> homeBases = gameSetup.gamePlan.getHomeBasePositions();
        startPossition = homeBases.get(myId);

    }

    @Override
    public Action makeMove(GameState gameState) {

        Agent myAgent = gameState.getAgentOfPlayer(myId);
        Point currentPosition = myAgent.getPosition();

        try {
            if (currentPosition.equals(startPossition)) {
                decideStrategy(currentPosition, gameState);
            }
        } catch (Exception ex) {
        }

        int round = gameState.getCurrentRound();
        return walkBasedOnStrategy(round - 1);
    }

    public void decideStrategy(Point currentPosition, GameState gameState) {
        int x = currentPosition.x;
        int y = currentPosition.y;

        int upLeft = calculatePoints(x - 2, y + 2, gameState);
        int upRight = calculatePoints(x + 2, y + 2, gameState);
        int downLeft = calculatePoints(x - 2, y - 2, gameState);
        int downRight = calculatePoints(x + 2, y - 2, gameState);

        List<Integer> rounds = Arrays.asList(upLeft, upRight, downLeft, downRight);
        int best = Collections.max(rounds);

        if (upRight == best) strategy = 0;
        if (downLeft == best) strategy = 1;
        if (downRight == best) strategy = 2;
        if (upLeft == best) strategy = 3;

    }

    public int calculatePoints(int x, int y, GameState gameState) {

        int upLeft = gameState.getField(new Point(x - 1, y + 1)).getResourceCount();
        int up = gameState.getField(new Point(x, y + 1)).getResourceCount();
        int upRight = gameState.getField(new Point(x + 1, y + 1)).getResourceCount();

        int base = gameState.getField(new Point(x, y)).getResourceCount();
        int right = gameState.getField(new Point(x + 1, y)).getResourceCount();
        int left = gameState.getField(new Point(x - 1, y)).getResourceCount();

        int downLeft = gameState.getField(new Point(x - 1, y - 1)).getResourceCount();
        int down = gameState.getField(new Point(x, y - 1)).getResourceCount();
        int downRight = gameState.getField(new Point(x + 1, y - 1)).getResourceCount();

        int count = 0;
        count = count + upLeft + up + upRight;
        count = count + downLeft + down + downRight;
        count = count + right + left + base;

        try {
            int upStream = gameState.getField(new Point(x, y + 2)).getResourceCount()
                    + gameState.getField(new Point(x, y + 3)).getResourceCount();

            int leftStream = gameState.getField(new Point(x - 2, y)).getResourceCount()
                    + gameState.getField(new Point(x - 3, y)).getResourceCount();

            int rightStream = gameState.getField(new Point(x + 2, y)).getResourceCount()
                    + gameState.getField(new Point(x + 3, y)).getResourceCount();

            int downStream = gameState.getField(new Point(x, y - 2)).getResourceCount()
                    + gameState.getField(new Point(x, y - 3)).getResourceCount();

            return count + leftStream + rightStream + downStream + upStream;

        } catch (GamePlan.PointOutsideOfGamePlan exception) {
            return count;
        }
    }

    public Action walkBasedOnStrategy(int currentRound) {
        if (strategy % STRATEGIES == 0) {
            if (currentRound % ROUND_STEPS == 0) return Action.PLANT_BOMB;
            if (currentRound % ROUND_STEPS == 1) return Action.RIGHT;
            if (currentRound % ROUND_STEPS == 2) return Action.RIGHT;
            if (currentRound % ROUND_STEPS == 3) return Action.UP;
            if (currentRound % ROUND_STEPS == 4) return Action.UP;
        }

        if (strategy % STRATEGIES == 1) {
            if (currentRound % ROUND_STEPS == 0) return Action.PLANT_BOMB;
            if (currentRound % ROUND_STEPS == 1) return Action.LEFT;
            if (currentRound % ROUND_STEPS == 2) return Action.LEFT;
            if (currentRound % ROUND_STEPS == 3) return Action.DOWN;
            if (currentRound % ROUND_STEPS == 4) return Action.DOWN;
        }

        if (strategy % STRATEGIES == 2) {
            if (currentRound % ROUND_STEPS == 0) return Action.PLANT_BOMB;
            if (currentRound % ROUND_STEPS == 1) return Action.DOWN;
            if (currentRound % ROUND_STEPS == 2) return Action.DOWN;
            if (currentRound % ROUND_STEPS == 3) return Action.RIGHT;
            if (currentRound % ROUND_STEPS == 4) return Action.RIGHT;
        }

        if (strategy % STRATEGIES == 3) {
            if (currentRound % ROUND_STEPS == 0) return Action.PLANT_BOMB;
            if (currentRound % ROUND_STEPS == 1) return Action.UP;
            if (currentRound % ROUND_STEPS == 2) return Action.UP;
            if (currentRound % ROUND_STEPS == 3) return Action.LEFT;
            if (currentRound % ROUND_STEPS == 4) return Action.LEFT;
        }

        // Fallback
        return Action.DOWN;
    }

}
